﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupScript : MonoBehaviour
{
    float m_yAxisStep = 0.1f;

    // Update is called once per frame
    void Update()
    {
        // each time decrement the y coordinate until it goes below -4.5f and gets destroyed
        transform.position = new Vector2(transform.position.x, transform.position.y - m_yAxisStep);
        if (transform.position.y < -4.5f)
            Destroy(gameObject);
    }
}
