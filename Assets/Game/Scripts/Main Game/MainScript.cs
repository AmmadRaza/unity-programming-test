﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
// this represents the class where all the game's data needs to be in order to be serialized and deserialized
[Serializable]			// make sure it is serializable with this attribute
public class CentralData
{
    // all the data that we want to be saved and loaded
    public float m_powerUpProb;
    public int m_score;
    public int m_scorePerBrick;
    public bool m_levelEditorEnabled;
    public int[,] m_brickGrid;
    // this constructor copies all the data from the mainScript class
    public CentralData()
    {
        m_powerUpProb = MainScript.m_powerUpProb;
        m_score = MainScript.m_score;
        m_scorePerBrick = MainScript.m_scorePerBrick;
        m_levelEditorEnabled = MainScript.m_levelEditorEnabled;
        m_brickGrid = new int[13, 12];
        // loop through the mainScript grid to copy it all
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                m_brickGrid[i, j] = MainScript.m_brickGrid[i, j];
            }
        }
    }
}
// this class holds the game's current data, they are public and static
public class MainScript
{

    public static float m_powerUpProb = 0.08f;    //this defines how likely are we going to find powerups
    public static int m_score = 0;                    //score of the game
    public static int m_scorePerBrick = 10;       //how much each brick give score
                                                // booleans to indicate the game's state
    public static bool m_loadEnabled = false;
    public static bool m_levelEditorEnabled = false;
    public static bool m_powerupActivated = false;
    // this grid indicates the state of the bricks in the scene
    // -1 : no brick, 0 : blue, 1: green and so on
    public static int[,] m_brickGrid = new int[13, 12];
    // this is the default value of the brick and which we will put if the user doesn't load any previous game 
    // and doesn't uses level editor
    public static int[,] m_defaultGrid =
    {
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
        {0,0,1,1,2,2,3,3,4,4,5,5},
    };
    // mathemathical function to transform vectors (or pair coordinates) from grid space to world space
    public static Vector2 GridToSceneFunction(int x, int y)
    {
        return new Vector2(0.64f * x - 3.84f, 0.32f * y - 0.65f);
    }
    // function to print the grid, useful while debugging
    public static void PrintGrid()
    {
        Debug.Log("*********Grid*********");
        for (int i = 0; i < 13; i++)
        {
            string s = "";
            for (int j = 0; j < 12; j++)
            {
                s = s + "," + m_brickGrid[i, j].ToString();
            }
            Debug.Log(s);
        }
        Debug.Log("**********************");
    }
    // this function copies all the data from a centralData object
    public static void CopyData(CentralData data)
    {
        m_powerUpProb = data.m_powerUpProb;
        m_score = data.m_score;
        m_scorePerBrick = data.m_scorePerBrick;
        m_levelEditorEnabled = data.m_levelEditorEnabled;
        m_brickGrid = new int[13, 12];
        // loop through the centralData object grid to copy it all
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                m_brickGrid[i, j] = data.m_brickGrid[i, j];
            }
        }
    }
    // this function is called whenever the user clicks the save button
    public static void Save()
    {
        // intantiate a binaryFormatter object
        BinaryFormatter bf = new BinaryFormatter();
        // create a new binary file located automatically with Application.persistentDataPath
        FileStream file = File.Create(Application.persistentDataPath + "/gameData.dat");
        // instantiate a centralData object so that we can serialize the game's data
        CentralData gameData = new CentralData();
        // serialize that gameData and write to the binary file
        bf.Serialize(file, gameData);
        // finally close the file
        file.Close();
    }

    // this function is called when the game starts after the user clicks on the load button
    public static void Load()
    {
        // make sure that the file exists	
        if (File.Exists(Application.persistentDataPath + "/gameData.dat"))
        {
            // instantiate a binaryFormatter object
            BinaryFormatter bf = new BinaryFormatter();
            // open the binary file located automatically with Application.persistentDataPath
            FileStream file = File.Open(Application.persistentDataPath + "/gameData.dat", FileMode.Open);
            // instantiate a centralData object so that we can deserialize from the file and write the data to the object
            CentralData gameData = (CentralData)bf.Deserialize(file);
            // copy all data in the gameData to the mainScript static variables
            MainScript.CopyData(gameData);
            // finally close the file
            file.Close();
        }
        else
        {
            // if there is no such file, just alert user
            Debug.Log("no saved files");
        }
    }
    // function to initialize the static data in mainScript
    public static void ReinitializeData()
    {
        m_score = 0;
        m_brickGrid = new int[13, 12];
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                m_brickGrid[i, j] = m_defaultGrid[i, j];
            }
        }

    }

}
