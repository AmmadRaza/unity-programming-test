﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    public bool m_hasPowerup;
    public int m_gridCoordinateX;
    public int m_gridCoordinateY;

    // Use this for initialization
    void Start()
    {
        // we will set the hasPowerup boolean to a random value according to a given probability
        m_hasPowerup = (Random.value < MainScript.m_powerUpProb) ? true : false;
    }
}
