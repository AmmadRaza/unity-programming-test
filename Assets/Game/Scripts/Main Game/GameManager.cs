﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Game.Utility;

public class GameManager : Singleton<GameManager>
{
    public GameObject[] m_brickGameobjects;
    // Use this for initialization
    void Start()
    {
        // if the levelEditor has not been used
        if (!MainScript.m_levelEditorEnabled)
        {
            if (MainScript.m_loadEnabled)     // and if the user wants to load a previous game
                MainScript.Load();          // load the game data from the binary file
            else
            {
                // if the user doesn't load a previous game, the just initialize data
                MainScript.ReinitializeData();
            }
        }
        // instanciate the bricks from the mainScript grid
        GameObject currentBrick;
        // loop through the grid
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                // if the grid value is a valid brick value
                if (MainScript.m_brickGrid[i, j] >= 0 && MainScript.m_brickGrid[i, j] <= 6)
                {
                    // instantiate the brick at the given position
                    // use the gridToSceneFunction to transform the i and j coordinates from the grid space to
                    // the world space
                    currentBrick = Instantiate(m_brickGameobjects[MainScript.m_brickGrid[i, j]],
                                               MainScript.GridToSceneFunction(i, j), transform.rotation);
                    // add a brickScript to thebrick
                    currentBrick.AddComponent(Type.GetType("BrickScript"));
                    // and set the grid coordinates
                    currentBrick.GetComponent<BrickScript>().m_gridCoordinateX = i;
                    currentBrick.GetComponent<BrickScript>().m_gridCoordinateY = j;
                }
            }
        }
    }
}
