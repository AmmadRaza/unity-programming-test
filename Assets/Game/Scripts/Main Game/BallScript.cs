﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallScript : MonoBehaviour
{
    private Rigidbody2D m_rigidbody;
    public GameObject m_powerup, m_gameOverPanel;
    public AudioSource m_brickAudio, m_paddleAudio;
    // Use this for initialization
    void Start()
    {
        m_gameOverPanel.SetActive(false);
        // initialize ridgidbody of the ball and add an initial force to it
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_rigidbody.AddForce(new Vector2(100f, 200f));
    }

    private void EndGame()
    {
        foreach(BrickScript brick in FindObjectsOfType<BrickScript>())
        {
            if (brick.gameObject.activeInHierarchy)
            {
                return;
            }
        }

        m_gameOverPanel.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // if the ball is below -4.5f in y axis, then game lost
        if (transform.position.y < -4.5f)
        {
            MainScript.m_levelEditorEnabled = false;      // make sure the level editor boolean is set to false
            MainScript.m_loadEnabled = false;             // make sure the level editor boolean is set to false
            m_gameOverPanel.SetActive(true);    //GAMEOVER
        }

        EndGame();
    }

    private bool IsTooFast()
    {
        return m_rigidbody.velocity.y >= 4.5f || m_rigidbody.velocity.x >= 4.5f;
    }
    // this is called once the ball hits something
    void OnCollisionEnter2D(Collision2D collusion)
    {
        // make sure it hits a brick
        if (collusion.gameObject.tag == "Brick")
        {
            m_brickAudio.Play();
            
            EndGame();
            
            // increment score, get its grid coordinates and use them change its state in the mainScript Grid to -1 (no brick)
            MainScript.m_score += MainScript.m_scorePerBrick;
            int x = collusion.gameObject.GetComponent<BrickScript>().m_gridCoordinateX;
            int y = collusion.gameObject.GetComponent<BrickScript>().m_gridCoordinateY;
            MainScript.m_brickGrid[x, y] = -1;
            // if the brick has powerup inside of it, instantiate a powerup at the brick position
            if (collusion.gameObject.GetComponent<BrickScript>().m_hasPowerup)
            {
                Instantiate(m_powerup, transform);
            }
            // if powerup got in the last 3 minutes, make sure to destroy all nearby bricks
            if (MainScript.m_powerupActivated)
            {
                // first get all brick objects
                GameObject[] bricks = GameObject.FindGameObjectsWithTag("Brick");
                foreach (GameObject brick in bricks)
                {   // loop through them to get the nearest ones
                    Vector2 dist = collusion.gameObject.transform.position - brick.transform.position;
                    if (dist.magnitude <= 0.64f)
                    {       // if the current brick is near
                            // increment score, get its grid coordinates and use them change its state in the mainScript Grid to -1 (no brick)
                        MainScript.m_score += MainScript.m_scorePerBrick;
                        int x1 = brick.GetComponent<BrickScript>().m_gridCoordinateX;
                        int y2 = brick.GetComponent<BrickScript>().m_gridCoordinateY;
                        MainScript.m_brickGrid[x1, y2] = -1;
                        // destroy the current brick
                        Destroy(brick);
                    }

                }
            }
            // finally destroy the brick
            Destroy(collusion.gameObject);
        }
        //when hit the paddle
        else if (collusion.gameObject.tag.Equals("Paddle"))
        {
                m_paddleAudio.Play();
    
            //is ball too fast
            if (IsTooFast())
                return;

            //make ball move faster
            m_rigidbody.velocity *= 1.1f;
        }
    }
}
