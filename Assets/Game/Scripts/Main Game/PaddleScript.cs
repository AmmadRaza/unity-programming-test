﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Utility;

public class PaddleScript : Singleton<PaddleScript>
{
    private float m_paddleMaxX;
    private float m_paddleMinX;
    private float m_borderlimit;
    public float m_speed;
    private Rigidbody2D m_rigidbody;
    private IEnumerator m_coroutine;
    // Use this for initialization

    void Awake()
    {
        m_rigidbody = GetRigidBody();

        AdjustResolution();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsMouseInUse())
        {
            Vector2 mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            transform.position = new Vector2(mousePos.x, transform.position.y);
        }
        else if (IsKeyboardInUse())
        {
            m_rigidbody.velocity = new Vector2(Input.GetAxis("Horizontal") * m_speed, 0);
        }
        else
        {
            m_rigidbody.velocity = Vector3.zero;
        }

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, m_paddleMinX, m_paddleMaxX), transform.position.y);
    }

    protected void AdjustResolution()
    {
        //Screen.width
        var resolution = GetCamScreenSize();

        var border = GameObject.Find("RightBorder").transform;

        m_borderlimit = (transform.position - border.position).magnitude;
        m_paddleMaxX = resolution.x / m_borderlimit*4.5f;
        m_paddleMinX = -resolution.x / m_borderlimit*4.5f;
    }

    private Rigidbody2D GetRigidBody()
    {
        var a_rigidbody = GetComponent<Rigidbody2D>();

        return a_rigidbody;
    }

    protected bool IsKeyboardInUse()
    {
        return (!Input.GetAxis("Horizontal").Equals(0));
    }

    protected bool IsMouseInUse()
    {
        return (!Input.GetAxis("Mouse X").Equals(0)
                ||
                !Input.GetAxis("Mouse X").Equals(0)
                ||
                !Input.GetAxis("Mouse X").Equals(0)
                   &&
                !Input.GetAxis("Mouse X").Equals(0));
    }

    private Vector3 GetCamScreenSize()
    {
        Vector3 resolution = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

        return resolution;
    }

    // this will be called each time the paddle hits a trigger object
    void OnTriggerEnter2D(Collider2D col)
    {
        // make sure the object hit is a powerup object
        if (col.gameObject.tag == "Powerup")
        {
            // if so we will start a coroutine to activate the powerup
            //Debug.Log ("powerup");
            m_coroutine = ActivatePowerup();
            StartCoroutine(m_coroutine);
            // destroy the powerup object at the end
            Destroy(col.gameObject);
        }

    }
    // this is the function to be called to activate powerup
    private IEnumerator ActivatePowerup()
    {
        // first set the boolean to true
        MainScript.m_powerupActivated = true;
        // wait for 3 minutes
        yield return new WaitForSeconds(180f);
        // and reset to false
        MainScript.m_powerupActivated = false;
    }
}
