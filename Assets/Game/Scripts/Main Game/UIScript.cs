﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIScript : MonoBehaviour
{
    // function to format the score text ( we want it to have a fixed 6 characters length)
    string Format(int value)
    {
        string res = value.ToString();
        while (res.Length < 6)
            res = "0" + res;
        return res;
    }

    void OnGUI()
    {
        // add a button save
        if (GUI.Button(new Rect(100f, 200f, 100f, 30f), "Save"))
        {
            // if clicked, call the mainScript method save
            MainScript.Save();
        }

    }
    // Update is called once per frame
    void Update()
    {
        // each time update the score text from the mainScript score and format it
        GameObject.Find("Txt_Score").GetComponent<Text>().text = Format(MainScript.m_score);

    }
}
