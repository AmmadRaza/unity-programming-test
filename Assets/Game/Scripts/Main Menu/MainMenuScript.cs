﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    //functions for buttons
    public void PlayButtonOnClick()
    {
        SceneManager.LoadScene("Main Game", LoadSceneMode.Single);
    }

    public void LoadButtonOnClick()
    {
        MainScript.m_loadEnabled = true;
        SceneManager.LoadScene("Main Game", LoadSceneMode.Single);
    }

    public void LevelEditorButtonOnClick()
    {
        SceneManager.LoadScene("Level Editor", LoadSceneMode.Single);
    }
}
