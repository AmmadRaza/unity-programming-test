﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ButtonManager : MonoBehaviour
{
    public GameObject m_buttonGameObject;
    public RectTransform m_parent;
    // this function submits the level editor once button clicked
    public void DoneButtonOnClick()
    {
        // copy the level editor grid to the mainScript grid
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                MainScript.m_brickGrid[i, j] = LevelEditorMainScript.m_brickGrid[i, j];
            }
            // enable the level editor
            MainScript.m_levelEditorEnabled = true;
            MainScript.m_score = 0;
            // finally load the scene
            SceneManager.LoadScene("Main Game", LoadSceneMode.Single);
        }
    }
    void Start()
    {
        // at the start, append buttons grid and initialize them
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                // initialize their values in the grid
                LevelEditorMainScript.m_brickGrid[i, j] = -1;
                // instantiate them
                GameObject currentButton = Instantiate(m_buttonGameObject);
                // set parent
                currentButton.GetComponent<RectTransform>().SetParent(m_parent, false);
                // set the local scale
                currentButton.GetComponent<RectTransform>().transform.localScale = new Vector2(1f, 1f);
                // set the position while converting the world position to screen position (used to position canvas)
                currentButton.GetComponent<RectTransform>().position =
                    Camera.main.WorldToScreenPoint(
                                     LevelEditorMainScript.GridToSceneFunction(i, j) + new Vector2(0f, -2f)
                    );
                // set the button's x and y coordinates
                currentButton.GetComponent<ButtonScript>().m_gridCoordinateX = i;
                currentButton.GetComponent<ButtonScript>().m_gridCoordinateY = j;
            }
        }
    }
}