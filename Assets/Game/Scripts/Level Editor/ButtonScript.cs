﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    int m_spriteCounter = 0;
    public int m_gridCoordinateX = -1;
    public int m_gridCoordinateY = -1;
    public Sprite[] m_buttonSprites;
    // this is called each time the button is clicked
    void ChangeSprite()
    {
        // we increment counter in order to change the sprite of the button
        m_spriteCounter++;

        if (m_spriteCounter == 7)         // make sure it  doesn't exceed 6
            m_spriteCounter = 0;
        // if the x and y coordinates are defined, use them to change the grid value
        // -1 : no brick, 0: blue brick, 1 : green brick and so on
        if (m_gridCoordinateX != -1 && m_gridCoordinateY != -1)
            LevelEditorMainScript.m_brickGrid[m_gridCoordinateX, m_gridCoordinateY] = m_spriteCounter - 1;
        // change the sprite of the button
        // 0 : no brick, 1: blue brick, 2 : green brick and so on
        gameObject.GetComponent<Image>().sprite = m_buttonSprites[m_spriteCounter];
    }
    // Use this for initialization
    void Start()
    {
        // initialize the button
        // set the sprite to the first one from the array
        gameObject.GetComponent<Image>().sprite = m_buttonSprites[0];
        // add an onclick event listener to the button (the methode changeSprite)
        gameObject.GetComponent<Button>().onClick.AddListener(ChangeSprite);
    }
}
