﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class LevelEditorMainScript : MonoBehaviour
{
    public static int[,] m_brickGrid = new int[13, 12];       // the temporary bricks grid for the level editor
                                                            // function to print the grid (useful in debugging)
    public static void PrintGrid()
    {
        Debug.Log("*********Grid*********");
        for (int i = 0; i < 13; i++)
        {
            string s = "";
            for (int j = 0; j < 12; j++)
            {
                s = s + "," + m_brickGrid[i, j].ToString();
            }
            Debug.Log(s);
        }
        Debug.Log("**********************");
    }
    // mathemathical function to transform vectors (or pair coordinates) from grid space to world space
    public static Vector2 GridToSceneFunction(int x, int y)
    {
        return new Vector2(0.64f * x - 3.84f, 0.32f * y - 0.65f) * 2f;
    }
}
