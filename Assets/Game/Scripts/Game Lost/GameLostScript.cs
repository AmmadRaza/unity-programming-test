﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameLostScript : MonoBehaviour
{
    public Text m_text;
    public void MainMenuButtonOnClick()
    {
        // load Main menu once player clicks on the button
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        // print out the score
        m_text.text = "score :" + MainScript.m_score.ToString();
    }
}
